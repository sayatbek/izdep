#Authors

@Sayatbek
ISTEP Solutions
Almaty, Kazakhstan
2017

#With assistance of 

Zhakhan Yergali

#Clone the source code
Go to:

https://bitbucket.org/sayatbek/izdep

or run this bash command:

```
$ git clone https://sayatbek@bitbucket.org/sayatbek/izdep.git
```

#Izdep.kz project

This project was designed by a student of Internatioanl IT University based in Almaty
as a final diploma project to defence his Bachelor degree. The aim of this web-site is to 
help people finding needed information in kazakh language. This is the search engine 
used to process kazakh languaged information in the internet

#Running locally

Run as a SpringBootWebApplication