package com.izdep.app.runner.utils;

public class ApiConst {

    public static final String TABLE_ALL_WORDS = "ALL_WORDS";
    public static final String TABLE_WORDS = "words";
    public static final String TABLE_LINKS = "links";
    public static final String TABLE_IMAGES = "images";
    public static final String TABLE_WORDS_VS_LINKS = "wordsVsLinks";
    public static final String TABLE_WORDS_VS_IMAGES = "wordsVsImages";

    public static final String JDBC_DRIVERS = "jdbc.drivers";
    public static final String JDBC_PASSWORD = "jdbc.password";
    public static final String JDBC_URL = "jdbc.url";
    public static final String JDBC_USER = "jdbc.username";

    public static final String CRAWLER_NEXT_IMAGE_URL_ID = "crawler.NextImageURLID";
    public static final String CRAWLER_NEXT_URL_ID_SCANNED = "crawler.NextURLIDScanned";
    public static final String CRAWLER_NEXT_URL_ID = "crawler.NextURLID";
    public static final String CRAWLER_NEXT_WORD_ID = "crawler.NextWordID";
    public static final String CRAWLER_DOMAIN = "crawler.domain";
    public static final String CRAWLER_MAX_URLS = "crawler.maxurls";
    public static final String CRAWLER_ROOT = "crawler.root";
    public static final String CRAWLER_RESET = "crawler.reset";

}
